import Vue from 'vue'
import VueRouter from 'vue-router'

Vue.use(VueRouter)

const routes = [
    {
        path: '/',
        name: 'Home',
        component: () => import('../views/home.vue')
    },
    {
        path: '/projects',
        name: 'Projects',
        component: () => import('../views/projects.vue')
    },
    {
        path: '/projects/WidGetGood',
        name: 'WidGetGood',
        component: () => import('../views/projects/WidGetGood')
    },{
        path: '/projects/UTBMsupport',
        name: 'UTBMsupport',
        component: () => import('../views/projects/UTBMsupport')
    },{
        path: '/projects/Legendenhaut',
        name: "Legend'EnHaut",
        component:() => import('../views/projects/Legendenhaut')
    },{
        path: '/projects/LoveLetter',
        name: "Love Letter",
        component:() => import('../views/projects/LoveLetter')
    },
    {
        path: '/projects/Gala',
        name: "Gala",
        component:() => import('../views/projects/Gala')
    },
    {
        path: '/projects/French-wine',
        name: 'french-wine',
        component:() => import('../views/projects/French-wine')

    }

]

const router = new VueRouter({
    routes,
    scrollBehavior () {
        return { x: 0, y: 0};
    }
})

export default router
